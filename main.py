#!/bin/python

from tkinter import *
from tkinter import filedialog
from tkinter import ttk
import openpyxl as xl
DEBUG=True
colors={
    "fg":"#000000",
    "mg":"#0000FF",
    "bg": "#CCCCDD",
    "activebackground":"#FFFFFF",
    "text":"000000"
}

class Application(Frame):
    def __init__(self, master=None):
        if DEBUG: print("Started Application")
        super().__init__(master)
        self.master = master
        self.pack()
        self.tree = None
        self.add_entries = None
        self.create_widgets()
        # Openpyxl Sheets
        self.wb = None
        self.master.configure(bg=colors["bg"])

    def open_wb(self):
        if DEBUG: print("Asking for file name")
        try:
            # Tries to open selected workbook
            wb_name = filedialog.askopenfilename(initialdir = ".",title = "Select file",filetypes = (("Excel Files","*.xlsx"),("All Files","*.*")))
            if DEBUG: print("File opened")
            self.wb = xl.load_workbook(wb_name)
            self.status_bar.config(text="File Successfully Loaded")
            if DEBUG: print("Loaded file into worksheet")
            self.show_sheet(self.wb.active)
            if DEBUG: print("Worksheet Active")
        except Exception as e:
            if DEBUG: print(e)
            # Load Error
            self.status_bar.config(text="File Load Error!")
            pass

    def create_widgets(self):
        self.master.title("Tablditor")
        self.master.geometry("700x700")
        # self.master.iconbitmap("./tablditor.ico")
        self.master.iconphoto(True, PhotoImage(file="./tablditor.png"))
        self.pack(fill=BOTH, expand=0)

        # Menu bar
        menu_bar = Menu(self.master,bg=colors["bg"],activebackground=colors["activebackground"],fg=colors["fg"])
        self.master.config(menu=menu_bar)

        file_menu = Menu(menu_bar,tearoff=0,bg=colors["bg"],activebackground=colors["activebackground"],fg=colors["fg"])
        menu_bar.add_cascade(label="File",menu=file_menu)
        file_menu.add_command(label="Open File", command=self.open_wb)
        file_menu.add_command(label="Quit", command=self.master.destroy)

        # Status Bar
        self.status_bar = Label(self.master,text="")
        self.status_bar.pack(side=BOTTOM)



    def make_tree(self,columns):
        # Add entries
        self.make_add_entries(columns)

        if DEBUG: print("Creating Tree View")
        # if tree exists delete
        if self.tree is not None:
            if DEBUG: print("Deleting old tree")
            self.tree.destroy()

        # Creates Tree View
        self.tree = ttk.Treeview(self.master, height=20, columns=columns, show='headings', selectmode='browse')
        self.tree.pack(side="top")

        # Add Header
        for c in range(len(columns)):
            self.tree.heading(c, text=columns[c])

    def make_add_entries(self,columns):
        if DEBUG: print("Creating Add Entry")
        # if add_entries exists delete
        if self.add_entries is not None:
            if DEBUG: print("Deleting old Add Entry")
            self.add_entries.destroy()

        self.add_entries = Frame(self.master,background=colors["bg"])
        self.add_entries.pack(side=LEFT,fill=Y)

        self.add_entries.miniframes = []
        for i in columns:
            miniframe = Frame(self.add_entries)
            miniframe.pack(side=TOP,fill=X)
            miniframe.config(background=colors["bg"])
            self.add_entries.miniframes.append(miniframe)

            tmplabel = Label(miniframe,text=i,bg=colors["bg"],fg=colors["fg"])
            tmplabel.pack(side=LEFT)
            miniframe.label = i

            tmpentry = Entry(miniframe,show=None,bg=colors["bg"],fg=colors["fg"])
            tmpentry.pack(side=RIGHT)
            miniframe.entry = tmpentry
        submit = Button(self.add_entries,text="Submit",command=self.submit)
        self.add_entries.submit = submit
        submit.pack(side=TOP)

    def submit(self):
        print("subb")
        for i in self.add_entries.winfo_children():
            print(i is Frame)
            print(i == Frame)
            print(type(i))
            print(type(Frame))
            if type(i) == type(Frame):
                print("yes")

    def show_sheet(self,ws):
        if DEBUG: print("Showing sheet")
        # Shows worksheet in tree view

        # Clears Tree View
        ws_list = []
        for i in ws:
            tmp = []
            for j in i:
                tmp.append(j.value)
            ws_list.append(tmp)
        if DEBUG: print("Sheet list created")

        tempcols = []
        for c in ws_list[0]:
            tempcols.append(c)
        self.make_tree(tempcols)

        if DEBUG: print("got list")
        tempList = ws_list[1:]
        for i in range(len(tempList)):
            self.tree.insert("","end",values=tempList[i])

root = Tk()
app = Application(master=root)
app.mainloop()
