# tablditor

Tablditor is a simple free and open source excel editor written in python.

## How to Use

Run the main.py file with python `python -m /path/to/main.py`. Open a file with the menu `File > Open`
